package programmes;

public class Programme1Ex5 {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // C'est un tableau de tableaux d'entiers
       // Plus exactement un tableau de 3 tableaux de 5 entiers
       
   
       // Affichage de la somme des éléments du tableau avec boucle indicées
      
       System.out.println();
       
       int somme=0;
       
       for ( int lig=0; lig<3; lig++){
       
           for( int col=0;col<5;col++){
           
               somme+=tableau[lig][col];
           }
       }
       
       System.out.printf("\nLa somme des élements du tableau vaut: %4d \n\n",somme);
    }
}
