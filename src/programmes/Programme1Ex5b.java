package programmes;

public class Programme1Ex5b {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // C'est un tableau de tableaux d'entiers
       // Plus exactement un tableau de 3 tableaux de 5 entiers
       
   
       // Affichage de la somme des éléments du tableau avec boucles for each
      
       System.out.println();
       
       int somme=0;
       
       for ( int[] ligTab : tableau){
       
           for( int x: ligTab){
           
               somme+=x;
           }
       }
       
       System.out.printf("\nLa somme des élements du tableau vaut: %4d \n\n",somme);
    }
    
}
