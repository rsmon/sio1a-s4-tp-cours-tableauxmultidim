package programmes;

public class Programme1Ex6 {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // C'est un tableau de tableaux d'entiers
       // Plus exactement un tableau de 3 tableaux de 5 entiers
       
   
       // Affichage de la moyenne de de chaque ligne  du tableau 
      
       System.out.println();
       
       for ( int lig=0; lig<tableau.length; lig++){
       
           float moyenne=0;
           int somme=0;
           for( int col=0;col<tableau[lig].length;col++){
           
               somme+=tableau[lig][col];
           }
           
           moyenne=(float)somme/tableau[lig].length;
           
            System.out.printf("Moyenne ligne %2d: %5.2f\n",
                    lig,        
                    moyenne
            ); 
       }
       
       System.out.println();
      
    }
}
