package programmes;

public class Programme1Ex2 {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // C'est un tableau de tableaux d'entiers
       // Plus exactement un tableau de 3 tableaux de 5 entiers
       
       // Affichage de la valeur située dans la ligne d'indice 2 et la colonne d'indice 3
       
       System.out.printf(
               
                   "\nValeur située dans la ligne d'indice 2 et la colonne d'indice 3: %2d\n\n",
                   tableau[2][3]
       );
       
    }
}
