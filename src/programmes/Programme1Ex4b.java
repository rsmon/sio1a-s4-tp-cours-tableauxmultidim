package programmes;

public class Programme1Ex4b {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // C'est un tableau de tableaux d'entiers
       // Plus exactement un tableau de 3 tableaux de 5 entiers
       
     
      // Affichage  des éléments de la ligne d'indice 1   avec une boucle for each
        
        System.out.println(); 
        System.out.println("Affichage  des éléments de la ligne d'indice 1 avec une  boucle for each\n");
        
        for( int x : tableau[1]){
        
            System.out.printf("%4d",x);            
        }
        
        System.out.println("\n");
        

    }
}
