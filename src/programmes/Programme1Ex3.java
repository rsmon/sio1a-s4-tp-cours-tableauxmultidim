package programmes;

public class Programme1Ex3 {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // C'est un tableau de tableaux d'entiers
       // Plus exactement un tableau de 3 tableaux de 5 entiers
       
   
       // Affichage du contenu du tableau
      
       System.out.println();
       
       for ( int lig=0; lig<3; lig++){
       
           for( int col=0;col<5;col++){
           
               System.out.printf("%3d", tableau[lig][col]);
           }
          
           System.out.println();
       }
       
       System.out.println();
    }
}
