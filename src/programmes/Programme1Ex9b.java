package programmes;

import java.util.Scanner;

public class Programme1Ex9b {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
      
       // Affichage des éléments d'une colonne d'indice donné
       
      
       System.out.println();
       System.out.println("Affichage des éléments de la colonne d'indice 2");
      
       int col=0;
       
       Scanner clavier=new Scanner(System.in);
       System.out.println("Colonne à afficher?");
       col=clavier.nextInt();
        
       
       for(int lig=0;lig<tableau.length; lig++ ){
       
            int decalage=4*col+1;
            
            String formatAff="%-"+decalage+"s"+"%3d\n";
            
            System.out.printf(formatAff," ",tableau[lig][col]);
       }
        
       System.out.println();
    }
}
