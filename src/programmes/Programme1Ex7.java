package programmes;

public class Programme1Ex7 {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // Affichage  du minimum de chaque ligne  du tableau 
      
       System.out.println();
        
       for ( int lig=0; lig<tableau.length; lig++){
       
           int miniLigne =999999;
       
           for( int col=0;col<tableau[lig].length;col++){
           
               if(tableau[lig][col]<miniLigne){miniLigne=tableau[lig][col];}
           }
         
           System.out.printf("Minimum ligne %2d: %2d\n",
                    lig,        
                    miniLigne
           ); 
       }
       
       System.out.println();
      
    }
}
