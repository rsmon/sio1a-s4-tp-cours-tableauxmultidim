
package programmes;

public class Programme2Ex2 {

    public static void main(String[] args) {
       
       // Une ligne par équipe
       // les équipes n'ont pas toutes la même taille, l'une d'entr'elles est vide 
        
       String tableauPersonnes[][] ={
       
           {"Pierre",   "Sophie",  "Elise" },
           {"Patrick",  "Bruno" ,  "Martine","Joséphine","Sarah"},
           {"Laurent",  "Fabienne","Alain"},
           {},
           {"Jean-Marc","Béatrice","Blandine"},
           {"Valentine"}
       
       }; 
        
       // Affichage de la composition de l'équipe 1
        
       System.out.println("\nComposition de l'équipe d'indice 1:\n"); 
       
       for(int i=0;i< tableauPersonnes[1].length;i++){
       
           System.out.println(tableauPersonnes[1][i]);
       }
       
       System.out.println("\n");
       
      
    }
}



