package programmes;

public class Programme1Ex8 {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // Affichage  du maximum  du tableau 
      
       System.out.println();
       
       int maxiTableau =-999999;
      
       for ( int lig=0; lig<tableau.length; lig++){
       
           for( int col=0;col<tableau[lig].length;col++){
           
               if(tableau[lig][col] > maxiTableau){maxiTableau=tableau[lig][col];}
           }   
       }
       
       System.out.printf("Maximum du tableau: %2d\n", maxiTableau); 
       
       System.out.println();
    }
}
