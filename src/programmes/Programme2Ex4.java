package programmes;

public class Programme2Ex4 {

    public static void main(String[] args) {
       
       // Une ligne par équipe
       // les équipes n'ont pas toutes la même taille, l'une d'entr'elles est vide 
        
       String tableauPersonnes[][] ={
           
           {"Pierre",   "Sophie",  "Elise" },
           {"Patrick",  "Bruno" ,  "Martine","Joséphine","Sarah"},
           {"Laurent",  "Fabienne","Alain"},
           {},
           {"Jean-Marc","Béatrice","Blandine"},
           {"Valentine"}
       
       }; 
       
       // Afficher la composition des équipes dont l'effectif est 3

        System.out.println("\nComposition des equipes dont l'effectif est 3:\n");
       
       for( int indEquipe=0;  indEquipe<tableauPersonnes.length;indEquipe++){
       
           if( tableauPersonnes[indEquipe].length==3){
             
               System.out.printf("Equipe %d: ",indEquipe);
               
               for(int  indNom=0 ; indNom<tableauPersonnes[indEquipe].length;indNom++){
                 
                   System.out.printf("%-15s",tableauPersonnes[indEquipe][indNom]);
               }
               System.out.printf("\n");
           }          
        }
        
        System.out.println();
    }
}



