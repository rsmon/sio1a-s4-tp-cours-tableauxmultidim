package programmes;

public class Programme1Ex3b {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31 },
           { -26,  24, -10, 15 },
           {   9, -43,   7, 56, -22 }
       
       };
       
       // C'est un tableau de tableaux d'entiers
       // Ce tableau comporte 4 lignes
       // Maus chaque ligne n'a pas le même nombre d'éléments
       
   
       // Affichage du contenu du tableau
      
       System.out.println();
       
       for ( int lig=0; lig<tableau.length; lig++){
       
           for( int col=0;col<tableau[lig].length;col++){
           
               System.out.printf("%3d", tableau[lig][col]);
           }
          
           System.out.println();
       }
       
       System.out.println();
    }
}
