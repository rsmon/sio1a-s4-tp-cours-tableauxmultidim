package programmes;

public class Programme1Ex9 {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
      
       // Affichage des éléments de la colonne d'indice 2
        
       for(int lig=0;lig<tableau.length; lig++ ){
      
           System.out.printf("%3d\n",tableau[lig][2]);
       }
        
       System.out.println();
    }
}
