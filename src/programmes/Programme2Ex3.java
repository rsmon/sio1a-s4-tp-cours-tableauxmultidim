
package programmes;

public class Programme2Ex3 {

    public static void main(String[] args) {
       
       // Une ligne par équipe
       // les équipes n'ont pas toutes la même taille, l'une d'entr'elles est vide 
        
       String tableauPersonnes[][] ={
       
           {"Pierre",   "Sophie",  "Elise" },
           {"Patrick",  "Bruno" ,  "Martine","Joséphine","Sarah"},
           {"Laurent",  "Fabienne","Alain"},
           {},
           {"Jean-Marc","Béatrice","Blandine"},
           {"Valentine"}
       
       }; 
       

       // Affichage de la composition de l'équipe d'indice 4 en utilisant un boucle for each
       
       System.out.println("\nComposition de l'équipe d'indice 4\n");  
        
       for (String pers : tableauPersonnes[4] ){
       
           System.out.println(pers);
       }
       
        System.out.println();
       
    }
}



