
package programmes;

public class Programme2Ex1 {

    public static void main(String[] args) {
       
       // Une ligne par équipe
       // les équipes n'ont pas toutes la même taille, l'une d'entr'elles est vide 
        
       String tableauPersonnes[][] ={
       
           {"Pierre",   "Sophie",  "Elise" },
           {"Patrick",  "Bruno" ,  "Martine","Joséphine","Sarah"},
           {"Laurent",  "Fabienne","Alain"},
           {},
           {"Jean-Marc","Béatrice","Blandine"},
           {"Valentine"}
       
       }; 
       
      
       // Affichage des effectifs de chaque équipe
       
       System.out.println();
        
       for( int eq=0; eq<tableauPersonnes.length; eq++){
       
           System.out.printf("Equipe: %2d Effectif: %2d\n",eq,tableauPersonnes[eq].length);
       }
       
       System.out.println();
    }
}



