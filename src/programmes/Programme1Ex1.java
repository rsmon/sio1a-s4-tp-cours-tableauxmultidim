package programmes;

public class Programme1Ex1 {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // C'est un tableau de tableaux d'entiers
       // Plus exactement un tableau de 3 tableaux de 5 entiers
       
               
       // Affichage du nombre de lignes du tableau
       
       System.out.printf("\nNombre de lignes du tableau: %d\n\n",tableau.length );
     
    }
}
