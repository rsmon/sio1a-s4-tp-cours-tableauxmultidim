package programmes;

public class Programme1Ex3c {

    public static void main(String[] args) {
    
       int tableau[][]= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31 },
           { -26,  24, -10, 15 },
           {   9, -43,   7, 56, -22 }
       
       };
       
       // C'est un tableau de tableaux d'entiers
       // Ce tableau comporte 4 lignes
       // Mais chaque ligne n'a pas le même nombre d'éléments
       
   
       // Affichage du contenu du tableau en n'utilisant que des boucles for each 
      
       System.out.println();
       
       for ( int[] lig: tableau){
       
           for( int x:lig){
           
               System.out.printf("%3d", x);
           }
          
           System.out.println();
       }
       
       System.out.println();
    }
}
